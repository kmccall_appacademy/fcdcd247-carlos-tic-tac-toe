class HumanPlayer
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts("Where do you want to move?")
    gets.chomp.split(',').map(&:to_i)
  end

  def display(board)
    board.grid[0].length.times do |i|
      line = board.grid[i].map { |x| if x.nil? then " " else x.to_s[0] end }
      puts(line)
    end
  end


end
