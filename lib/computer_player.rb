class ComputerPlayer
  attr_accessor :name, :board, :mark, :grid

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    size = @board.grid[0].length
    # horizontal
    size.times do |i|
      if [[@mark, nil], [nil, @mark]].include?(@board.grid[i].uniq) && @board.grid[i].count(nil) == 1
        return [i, @board.grid[i].index(nil)]
      end
    end
    # vertical
    size.times do |i|
      column = (0...size).map { |j| @board.grid[j][i] }
      if [[@mark, nil], [nil, @mark]].include?(column.uniq) && column.count(nil) == 1
        return [column.index(nil), i]
      end
    end
    # left diagonal
    left_diag = (0...size).map { |i| @board.grid[i][i] }
    if [[@mark, nil], [nil, @mark]].include?(left_diag.uniq) && left_diag.count(nil) == 1
      return [left_diag.index(nil), left_diag.index(nil)]
    end
    # right diagonal
    right_diag = (0...size).map { |i| @board.grid[i][size - i - 1] }
    if [[@mark, nil], [nil, @mark]].include?(right_diag.uniq) && right_diag.count(nil) == 1
      return [right_diag.index(nil), size - right_diag.index(nil) - 1]
    end
    # no winning move; do a random one
    moves = []
    size.times do |i|
      size.times do |j|
        if @board.grid[i][j].nil?
          moves << [i, j]
        end
      end
    end
    moves.sample
  end

end
