class Board
  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def winner
    marks = @grid.flatten.uniq.reject(&:nil?)
    size = grid[0].length
    marks.each do |mark|
      # horizontal
      size.times do |i|
        if @grid[i].all? { |posmark| posmark == mark }
          return mark
        end
      end
      # vertical
      size.times do |i|
        column = (0...size).map { |j| @grid[j][i] }
        if column.all? { |posmark| posmark == mark }
          return mark
        end
      end
      # left diagonal
      left_diag = (0...size).map { |i| @grid[i][i] }
      if left_diag.all? { |posmark| posmark == mark }
        return mark
      end
      # right diagonal
      right_diag = (0...size).map { |i| @grid[i][size - i - 1] }
      if right_diag.all? { |posmark| posmark == mark }
        return mark
      end
    end
    nil
  end

  def over?
    !(self.winner.nil? && @grid.flatten.any?(&:nil?))
  end

end
